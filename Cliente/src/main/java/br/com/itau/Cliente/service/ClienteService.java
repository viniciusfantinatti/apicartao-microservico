package br.com.itau.Cliente.service;

import br.com.itau.Cliente.exceptions.ClienteNotFoundException;
import br.com.itau.Cliente.models.Cliente;
import br.com.itau.Cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente incluirCliente(Cliente cliente){
        Cliente objCliente = clienteRepository.save(cliente);

        return objCliente;
    }

    public Cliente buscarClientePorId(Integer id){
        Optional<Cliente> optCliente = clienteRepository.findById(id);

        if(!optCliente.isPresent()){
            throw new ClienteNotFoundException();
        }
        return optCliente.get();
    }
}
