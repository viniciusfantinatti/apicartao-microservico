package br.com.itau.Cartao.controllers;

import br.com.itau.Cartao.DTO.*;
import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;


    //INCLUSAO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CadastraCartaoSaida incluirCartao(@RequestBody @Valid CadastraCartaoEntrada cadastraCartaoEntrada){
        Cartao objCartao = cartaoMapper.toCartao(cadastraCartaoEntrada);

        objCartao = cartaoService.incluirCartao(objCartao);

        return cartaoMapper.toCadastraCartaoSaida(objCartao);
    }

    //ATUALIZA
    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public AtualizaCartaoSaida atualizaCartao(@PathVariable(name = "numero") String numero,
                                              @RequestBody AtualizaCartaoEntrada atualizaCartaoEntrada){

        atualizaCartaoEntrada.setNumero(numero);

        Cartao objCartao = cartaoMapper.toCartao(atualizaCartaoEntrada);

        objCartao = cartaoService.atualizaCartao(objCartao);

        return cartaoMapper.toAtualizaCartaoSaida(objCartao);
    }


    //CONSULTA
    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public ConsultaCartaoSaida consultarCartaoPorNumero(@PathVariable String numero){
        Cartao objCartao = cartaoService.consultarCartaoPorNumero(numero);

        return cartaoMapper.toConsultaCartaoSaida(objCartao);
    }

    //CONSULTA
    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ConsultaCartaoSaida consultarCartaoPorId(@PathVariable Integer id){
        Cartao objCartao = cartaoService.consultarCartaoPorId(id);

        return cartaoMapper.toConsultaCartaoSaida(objCartao);
    }
}
