package br.com.itau.Cartao.DTO;

import javax.validation.constraints.NotNull;

public class AtualizaCartaoEntrada {

    private String numero;

    @NotNull
    private Boolean ativo;

    public AtualizaCartaoEntrada() {
    }

    public AtualizaCartaoEntrada(String numero, @NotNull Boolean ativo) {
        this.numero = numero;
        this.ativo = ativo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
