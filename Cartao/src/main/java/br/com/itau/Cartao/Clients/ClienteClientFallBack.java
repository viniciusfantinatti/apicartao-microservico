package br.com.itau.Cartao.Clients;

import br.com.itau.Cartao.models.Cliente;

public class ClienteClientFallBack implements ClienteClients {
    @Override
    public Cliente buscarClientePorId(Integer id) {
        Cliente clienteFallBack = new Cliente(999, "TESTE-FALLBACK");

        return clienteFallBack;
    }
}
