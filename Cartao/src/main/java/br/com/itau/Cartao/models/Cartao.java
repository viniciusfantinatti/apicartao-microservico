package br.com.itau.Cartao.models;

import javax.persistence.*;

@Entity
@Table
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private Integer id;

    @Column(length = 9)
    private String numero;

    @Column
    private Boolean ativo;

    @Column(length = 5)
    private  Integer clienteId;

    public Cartao() {
    }

    public Cartao(Integer id, String numero, Boolean ativo, Integer clienteId) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.clienteId = clienteId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
