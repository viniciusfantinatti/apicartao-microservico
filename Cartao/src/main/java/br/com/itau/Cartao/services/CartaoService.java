package br.com.itau.Cartao.services;

import br.com.itau.Cartao.Clients.ClienteClients;
import br.com.itau.Cartao.exceptions.CartaoNotFoundException;
import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.Cliente;
import br.com.itau.Cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;


    @Autowired
    private ClienteClients clienteClients;


    public Cartao incluirCartao(Cartao cartao){
        Cliente objCliente = clienteClients.buscarClientePorId(cartao.getClienteId());

        cartao.setClienteId(objCliente.getId());
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao atualizaCartao(Cartao cartao){
        Cartao objCartao = consultarCartaoPorNumero(cartao.getNumero());

        objCartao.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(objCartao);
    }

    public Cartao consultarCartaoPorId(Integer id){
        Optional<Cartao> optCartao = cartaoRepository.findById(id);

        if (!optCartao.isPresent()){

            throw new CartaoNotFoundException();
        }

        return optCartao.get();
    }

    public Cartao consultarCartaoPorNumero(String numero){
        Optional<Cartao> optCartao = cartaoRepository.findByNumero(numero);

        if (!optCartao.isPresent()){

            throw new CartaoNotFoundException();
        }

        return optCartao.get();
    }
}
