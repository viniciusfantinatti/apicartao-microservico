package br.com.itau.Cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cliente inválido - abobrinha.")
public class ClienteInvalidException extends RuntimeException {
}
