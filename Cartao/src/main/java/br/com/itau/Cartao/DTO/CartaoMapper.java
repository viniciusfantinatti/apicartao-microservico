package br.com.itau.Cartao.DTO;

import br.com.itau.Cartao.models.Cartao;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {


    public Cartao toCartao(CadastraCartaoEntrada cadastraCartaoEntrada) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cadastraCartaoEntrada.getNumero());
        cartao.setClienteId(cadastraCartaoEntrada.getClienteId());

        return cartao;
    }

    public CadastraCartaoSaida toCadastraCartaoSaida(Cartao cartao) {
        CadastraCartaoSaida cadastraCartaoSaida = new CadastraCartaoSaida();

        cadastraCartaoSaida.setId(cartao.getId());
        cadastraCartaoSaida.setNumero(cartao.getNumero());
        cadastraCartaoSaida.setClienteId(cartao.getClienteId());
        cadastraCartaoSaida.setAtivo(cartao.getAtivo());

        return cadastraCartaoSaida;
    }

    public Cartao toCartao(AtualizaCartaoEntrada atualizaCartaoEntrada) {
        Cartao cartao = new Cartao();

        cartao.setNumero(atualizaCartaoEntrada.getNumero());
        cartao.setAtivo(atualizaCartaoEntrada.getAtivo());

        return cartao;
    }

    public AtualizaCartaoSaida toAtualizaCartaoSaida(Cartao cartao) {
        AtualizaCartaoSaida atualizaCartaoSaida = new AtualizaCartaoSaida();

        atualizaCartaoSaida.setId(cartao.getId());
        atualizaCartaoSaida.setNumero(cartao.getNumero());
        atualizaCartaoSaida.setClienteId(cartao.getClienteId());
        atualizaCartaoSaida.setAtivo(cartao.getAtivo());

        return atualizaCartaoSaida;
    }

    public ConsultaCartaoSaida toConsultaCartaoSaida(Cartao cartao) {
        ConsultaCartaoSaida consultaCartaoSaida = new ConsultaCartaoSaida();

        consultaCartaoSaida.setId(cartao.getId());
        consultaCartaoSaida.setNumero(cartao.getNumero());
        consultaCartaoSaida.setClienteId(cartao.getClienteId());

        return consultaCartaoSaida;
    }
}
