package br.com.itau.Cartao.DTO;

public class ConsultaCartaoSaida {

    private Integer id;

    private String numero;

    private Integer clienteId;

    public ConsultaCartaoSaida() {
    }

    public ConsultaCartaoSaida(Integer id, String numero, Integer clienteId) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
