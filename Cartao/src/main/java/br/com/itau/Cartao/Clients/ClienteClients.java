package br.com.itau.Cartao.Clients;

import br.com.itau.Cartao.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
//@FeignClient(name="cliente", configuration = OAuth2FeignConfiguration.class)
public interface ClienteClients {

    @GetMapping("/cliente/{id}")
    Cliente buscarClientePorId(@PathVariable Integer id);
}
