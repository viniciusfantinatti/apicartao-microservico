package br.com.itau.Cartao.Clients.decoder;

import br.com.itau.Cartao.exceptions.ClienteInvalidException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientsDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            throw new ClienteInvalidException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
