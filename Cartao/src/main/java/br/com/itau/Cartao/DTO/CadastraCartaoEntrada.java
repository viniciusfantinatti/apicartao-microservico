package br.com.itau.Cartao.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CadastraCartaoEntrada {

    @NotBlank
    private String numero;

    @NotNull
    private Integer clienteId;


    public CadastraCartaoEntrada() {
    }

    public CadastraCartaoEntrada(String numero, Integer clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;

    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
