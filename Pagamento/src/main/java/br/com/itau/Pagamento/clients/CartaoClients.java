package br.com.itau.Pagamento.clients;

import br.com.itau.Pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CartaoClientConfiguration.class)
//@FeignClient(name="cartao", configuration = OAuth2FeignConfiguration.class)
public interface CartaoClients {

    @GetMapping("/cartao/id/{id}")
    Cartao consultarCartaoPorId(@PathVariable Integer id);
}
