package br.com.itau.Pagamento.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IncluirPagamentoSaida {

    private Integer id;

    @JsonProperty("cartao_id")
    private Integer cartaoId;

    private String descricao;

    private Double valor;

    public IncluirPagamentoSaida() {
    }

    public IncluirPagamentoSaida(Integer id, Integer cartaoId, String descricao, Double valor) {
        this.id = id;
        this.cartaoId = cartaoId;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Integer cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
