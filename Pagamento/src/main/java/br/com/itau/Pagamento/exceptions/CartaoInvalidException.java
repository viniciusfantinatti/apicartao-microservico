package br.com.itau.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Cartão invalido - abobrinha !")
public class CartaoInvalidException extends RuntimeException {
}
