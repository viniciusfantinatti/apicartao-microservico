package br.com.itau.Pagamento.clients;

import br.com.itau.Pagamento.clients.decoder.CartaoClientDecoder;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration extends OAuth2FeignConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CartaoClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new CartaoClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }
}
