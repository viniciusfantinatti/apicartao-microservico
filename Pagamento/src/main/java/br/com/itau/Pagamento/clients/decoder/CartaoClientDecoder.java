package br.com.itau.Pagamento.clients.decoder;


import br.com.itau.Pagamento.exceptions.CartaoInvalidException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            throw new CartaoInvalidException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
