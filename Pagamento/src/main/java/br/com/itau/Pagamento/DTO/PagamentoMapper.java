package br.com.itau.Pagamento.DTO;

import br.com.itau.Pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(IncluirPagamentoEntrada incluirPagamentoEntrada){
        Pagamento objPagto = new Pagamento();

        objPagto.setCartaoId(incluirPagamentoEntrada.getCartaoId());
        objPagto.setDescricao(incluirPagamentoEntrada.getDescricao());
        objPagto.setValor(incluirPagamentoEntrada.getValor());

        return objPagto;
    }

    public IncluirPagamentoSaida toIncluirPagamentoSaida(Pagamento pagamento){
        IncluirPagamentoSaida objIncluirPagtoSaida = new IncluirPagamentoSaida();

        objIncluirPagtoSaida.setId(pagamento.getId());
        objIncluirPagtoSaida.setCartaoId(pagamento.getCartaoId());
        objIncluirPagtoSaida.setDescricao(pagamento.getDescricao());
        objIncluirPagtoSaida.setValor(pagamento.getValor());

        return objIncluirPagtoSaida;
    }
}
