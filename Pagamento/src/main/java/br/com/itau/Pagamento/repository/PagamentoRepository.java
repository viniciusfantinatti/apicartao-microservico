package br.com.itau.Pagamento.repository;

import br.com.itau.Pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartaoId(Integer cartaoId);
}
