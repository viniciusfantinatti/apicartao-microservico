package br.com.itau.Pagamento.models;

public class Cartao {

    private Integer id;
    private String numero;
    private Boolean ativo;
    private Integer clienteId;

    public Cartao() {
    }

    public Cartao(Integer id, String numero, Boolean ativo, Integer clienteId) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.clienteId = clienteId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
