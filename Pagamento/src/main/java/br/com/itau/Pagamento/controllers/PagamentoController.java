package br.com.itau.Pagamento.controllers;

import br.com.itau.Pagamento.DTO.IncluirPagamentoEntrada;
import br.com.itau.Pagamento.DTO.PagamentoMapper;
import br.com.itau.Pagamento.models.Pagamento;
import br.com.itau.Pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    public Pagamento incluirPagamento(@Valid @RequestBody Pagamento pagamento) {
        Pagamento objPagto = pagamentoService.incluirPagamento(pagamento);

        return objPagto;
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<Pagamento> consultaPagamentosPorCartaoId(@PathVariable Integer cartaoId) {
        return pagamentoService.consultaPagtosPorIdCartao(cartaoId);
    }
}
