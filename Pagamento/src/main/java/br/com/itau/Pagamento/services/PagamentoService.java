package br.com.itau.Pagamento.services;

import br.com.itau.Pagamento.clients.CartaoClients;
import br.com.itau.Pagamento.models.Cartao;
import br.com.itau.Pagamento.models.Pagamento;
import br.com.itau.Pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClients cartaoClients;


    public Pagamento incluirPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoClients.consultarCartaoPorId(pagamento.getCartaoId());

        pagamento.setCartaoId(cartao.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> consultaPagtosPorIdCartao(Integer cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }


}
